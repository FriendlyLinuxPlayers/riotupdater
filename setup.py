#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name="riotupdater",
    version="0.0.1",
    description=r"A very minimal updater script for riot, the matrix client. Use at own your risk.",
    author="Stephan Lanfermann",
    author_email="stelanf@gmail.com",
    packages=find_packages(),
    url="https://gitlab.com/FriendlyLinuxPlayers/riotupdater",
    setup_requires=[line for line in open("requirements.txt", 'r').readlines()]
)
