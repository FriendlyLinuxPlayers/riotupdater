import re
import tarfile
from io import BytesIO
from sys import stderr
from tempfile import mkdtemp, mkstemp

import gnupg
import urllib.request as req

from os import getenv, listdir, symlink, rename, unlink
from git import Repo


STABLE_VERSION_REGEX = re.compile(r"^v\d+\.\d+\.\d+$")

PREFIX = "RIOTUPDATER_"

LINK_DIRNAME = getenv(PREFIX + "LINK_DIRNAME", "htdocs")
assert LINK_DIRNAME
REPO_DIR = getenv(PREFIX + "REPO_DIR")
assert REPO_DIR
TARGET_DIR = getenv(PREFIX + "TARGET_DIR")
assert TARGET_DIR
RELEASES_URL_TEMPLATE = getenv(
    PREFIX + "RELEASES_URL",
    r"https://github.com/vector-im/riot-web/releases/download/{version}/riot-{version}.tar.gz{extra_extension}"
)
assert RELEASES_URL_TEMPLATE
RIOT_RELEASE_KEY = getenv(PREFIX + "RIOT_RELEASE_KEY")
assert RIOT_RELEASE_KEY


def main():
    repo = Repo(path=REPO_DIR)
    origin = repo.remote()
    origin.fetch()

    latest_version = "v0.0.0"
    for name in listdir(TARGET_DIR):
        if STABLE_VERSION_REGEX.match(name) and name > latest_version:
            latest_version = name

    has_newer_version = False
    for tag in repo.tags:
        if STABLE_VERSION_REGEX.match(tag.name) and tag.name > latest_version:
            has_newer_version = True
            latest_version = tag.name

    if not has_newer_version:
        print("No newer version available, nothing to do!")
        exit()

    signature_url = RELEASES_URL_TEMPLATE.format(version=latest_version, extra_extension=".asc")
    with open(RIOT_RELEASE_KEY, mode="r") as release_key_file:
        release_key = release_key_file.read()
        with req.urlopen(signature_url) as sig:
            _, sig_file_name = mkstemp()
            with open(sig_file_name, mode="w") as sigout:
                sigout.write(sig.read().decode("utf8"))

            archive_url = RELEASES_URL_TEMPLATE.format(version=latest_version, extra_extension="")
            with req.urlopen(archive_url) as archive:
                gpg = gnupg.GPG(gnupghome=mkdtemp())
                import_ = gpg.import_keys(release_key)
                assert import_
                archive_io = BytesIO(archive.read())
                archive_io.seek(0)
                verified = gpg.verify_data(sig_file_name, archive_io.read())

                if not verified.valid:
                    print("Invalid signature for archive, aborting.", file=stderr)
                    exit(1)

                archive_io.seek(0)
                tar = tarfile.open(fileobj=archive_io, mode="r:gz")
                member: tarfile.TarInfo = tar.members[0]
                tar.extractall(TARGET_DIR)
                target_name = TARGET_DIR + "/" + latest_version
                rename(TARGET_DIR + "/" + member.name, target_name)
                link_name = TARGET_DIR + "/" + LINK_DIRNAME
                try:
                    unlink(link_name)
                except FileNotFoundError:
                    # Don't care if it doesn't exist, e.g. on first run
                    pass
                symlink(target_name, link_name)


if __name__ == '__main__':
    main()